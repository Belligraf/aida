import os

import torch
import numpy as np
import torch.nn.functional as torch_func

from cv2 import cv2

from constants import commands_dict, LABELS, IMG_SIZE
from learn import load_weight, net


def register_command(func) -> callable:
    commands_dict[func.__name__] = func
    return func


def get_key(val: int) -> str:
    for key in LABELS:
        if LABELS[key] == val:
            return key.split('/')[1]


@register_command
def get_music(filename: str):
    path = os.path.join(*filename.split('/'))
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
    data = torch.Tensor([np.array(img, dtype=object)]).view(-1, 50, 50)
    data /= 255.0
    net.eval()
    net_out = net(data.view(-1, 1, 50, 50))[0]  # returns a list,
    net_out = torch_func.softmax(net_out)
    predicted_class = torch.argmax(net_out)
    result = predicted_class.item()
    print(get_key(result), f'{round(net_out[result].item() * 100)}%')


@register_command
def help_me():
    print(*commands_dict.keys())


def main():
    np.random.seed(0)
    torch.manual_seed(0)
    torch.cuda.manual_seed(0)
    load_weight()
    print('you can write help_me')
    commands_dict['get_music']('img/classic.jpg')
    commands_dict['get_music']('img/jazz.jpg')
    commands_dict['get_music']('img/rock.jpg')
    # while True:
    #     command = input("Input command: ").split()
    #     if not command:
    #         break
    #     try:
    #         commands_dict[command[0]](*command[1:])
    #     except KeyError:
    #         print("command not found")
    #     except cv2.error:
    #         print("wrong arguments")

    print('exit')


if __name__ == '__main__':
    main()
