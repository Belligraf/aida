import torch
import torch.nn as nn

from constants import COUNT_GENRES


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1_1 = nn.Conv2d(1, 32, 3)
        self.conv1_2 = nn.Conv2d(32, 32, 3)
        self.act1 = nn.ReLU()
        self.batch_norm1 = torch.nn.BatchNorm2d(32)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2_1 = nn.Conv2d(32, 64, 3)
        self.conv2_2 = nn.Conv2d(64, 64, 3)
        self.act2 = nn.ReLU()
        self.batch_norm2 = torch.nn.BatchNorm2d(64)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3_1 = nn.Conv2d(64, 128, 3)
        self.conv3_2 = nn.Conv2d(128, 128, 3)
        self.act3 = nn.ReLU()
        self.batch_norm3 = torch.nn.BatchNorm2d(128)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv4_1 = nn.Conv2d(128, 256, 1)
        self.conv4_2 = nn.Conv2d(256, 256, 1)
        self.act4 = nn.ReLU()
        self.batch_norm4 = torch.nn.BatchNorm2d(256)
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.fc1 = nn.Linear(256, 256)
        self.act6 = nn.ReLU()
        self.batch_norm6 = torch.nn.BatchNorm1d(256)

        self.fc2 = torch.nn.Linear(256, 64)
        self.act7 = nn.ReLU()
        self.batch_norm7 = torch.nn.BatchNorm1d(64)

        self.fc3 = nn.Linear(64, COUNT_GENRES)  # 512 вход

    def forward(self, input_data):
        # print(input_data)
        out = self.conv1_2(self.conv1_1(input_data))
        out = self.act1(out)
        out = self.batch_norm1(out)
        out = self.pool1(out)

        out = self.conv2_2(self.conv2_1(out))
        out = self.act2(out)
        out = self.batch_norm2(out)
        out = self.pool2(out)

        out = self.conv3_2(self.conv3_1(out))
        out = self.act3(out)
        out = self.batch_norm3(out)
        out = self.pool3(out)

        out = self.conv4_2(self.conv4_1(out))
        out = self.act4(out)
        out = self.batch_norm4(out)
        out = self.pool4(out)

        out = out.view(out.size(0),
                       out.size(1) * out.size(2) * out.size(3))

        out = self.fc1(out)
        out = self.act6(out)
        out = self.batch_norm6(out)

        out = self.fc2(out)
        out = self.act7(out)
        out = self.batch_norm7(out)

        out = self.fc3(out)

        return out

    def save(self, path='save_weight'):
        torch.save(self.state_dict(), path)
        print('weights have been saved')
