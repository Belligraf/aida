import os

commands_dict = {}

REBUILD_DATA = False
BATCH_SIZE = 100
EPOCHS = 20
IMG_SIZE = 50
DATA_DIR = 'train/'
TEST_DIR = 'test/'
DIRS = os.listdir(DATA_DIR)
TEST_DIRS = os.listdir(TEST_DIR)
TEST_LABEL = {}
LABELS = {}
count = {}
COUNT_GENRES = len(DIRS)
