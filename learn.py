import os

import numpy as np
import torch
import warnings

from matplotlib import pyplot as plt
from tqdm import tqdm
from cv2 import cv2
from torch import optim, nn

from constants import REBUILD_DATA, TEST_LABEL, EPOCHS, BATCH_SIZE, count, DIRS, DATA_DIR, LABELS, TEST_DIRS, \
    TEST_DIR, IMG_SIZE
from neural_network import Net

net = Net()

warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)


def init_constants(dirs, data_dir: str, labels: dict):
    for genre, i in zip(dirs, range(len(dirs))):
        labels[data_dir + genre] = i
        count[data_dir + genre] = 0

    print(labels)


init_constants(DIRS, DATA_DIR, LABELS)
init_constants(TEST_DIRS, TEST_DIR, TEST_LABEL)


def get_data_from_img(file_name: str, label: str, labels: dict) -> list[float]:
    path = os.path.join(label, file_name)
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
    return [[np.array(img, dtype=object)], labels[label]]


def make_data(labels, save_name):
    data = []
    for label in labels:
        for file_name in tqdm(os.listdir(label)):
            if "jpg" in file_name:
                try:
                    data.append(get_data_from_img(file_name, label, labels))

                    count[label] += 1
                except FileNotFoundError:
                    print('error:', label, file_name)

    np.random.shuffle(data)
    np.save(save_name, data)
    print('Genre count:', count)


def get_id_max_val(arr):
    lst_num = list(enumerate(arr, 0))
    t_max = max(lst_num, key=lambda i: i[1])
    return t_max[0]


def train(train_net, input_train, out_train, input_test, out_test):
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    train_net = train_net.to(device)
    loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(train_net.parameters(), lr=1.0e-3)

    batch_size = 100

    test_accuracy_history = []
    test_loss_history = []

    input_test = input_test.to(device)
    out_test = out_test.to(device).type(torch.LongTensor)

    for epoch in range(EPOCHS):
        order = np.random.permutation(len(input_train))
        for start_index in range(0, len(input_train), batch_size):
            optimizer.zero_grad()
            train_net.train()

            batch_indexes = order[start_index:start_index + batch_size]

            input_batch = input_train[batch_indexes].to(device).view(-1, 1, 50, 50)
            out_batch = out_train[batch_indexes].to(device).type(torch.LongTensor)

            preds = train_net.forward(input_batch)

            loss_value = loss(preds, out_batch)
            loss_value.backward()

            optimizer.step()

        train_net.eval()
        test_preds = train_net.forward(input_test)
        test_loss_history.append(loss(test_preds, out_test).data.cpu())

        accuracy = (test_preds.argmax(dim=1) == out_test).float().mean().data.cpu()
        test_accuracy_history.append(accuracy)

        print('Epoch:', epoch + 1, 'Accuracy:', round(accuracy.item(), 3), 'Loss:', round(loss_value.item(), 3))
    print('---------------')
    return test_accuracy_history, test_loss_history


def init_data(save_name: str):
    data = np.load(save_name, allow_pickle=True)

    input_data = torch.Tensor([i[0][0] for i in data]).view(-1, 1, 50, 50)
    input_data = input_data / 255.0
    output_data = torch.Tensor([i[1] for i in data])

    return input_data, output_data


def learn():
    graph = {}

    if REBUILD_DATA:
        make_data(LABELS, "training_data.npy")
        make_data(TEST_LABEL, "test_data.npy")

    train_input, train_output = init_data("training_data.npy")
    test_input, test_output = init_data("test_data.npy")

    print('Size train input:', len(train_input))
    print('Size test input:', len(test_input))

    graph['accuracy'], graph['loss'] = train(net, train_input, train_output, test_input, test_output)

    loss = graph['loss']
    max_loss = max(loss)

    if max_loss.item() > 1:
        graph['loss'] = [i.item() / max_loss.item() for i in loss]

    for experiment_id in graph.keys():
        plt.plot(graph[experiment_id], label=experiment_id)
    plt.xlabel('Epoch')
    plt.ylabel('Value')
    plt.legend()
    plt.show()
    net.save()


def load_weight(path='save_weight'):
    try:
        net.load_state_dict(torch.load(path))
        print("weights have been loaded")
    except FileNotFoundError:
        print("no save weight\nstart learning")
        learn()
